Model converter alpha v0.1.4
============================

Modified by Knightmare for TGA skin export and MDL frame group conversion

The model converter is a command-line tool which can import MDL, MD2, or MD3 models, and export to MDL or MD2.

http://www.icculus.org/qshed/qwalk/

Usage:

modelconv [options] -i infilename outfilename
Output format is specified by the file extension of outfilename.

Options:
  -i filename        specify the model to load (required).
  -notex             remove all existing skins from model after importing.
  -tex filename      replace the model's texture with the given texture. This
                     is required for any texture to be loaded onto MD2 or MD3
                     models (the program doesn't automatically load external
                     skins). Supported formats are PCX, TGA, and JPEG.
  -texwidth #        see below
  -texheight #       resample the model's texture to the given dimensions.
  -skinpath x        specify the path that skins will be exported to when
                     exporting to md2 (e.g. "models/players"). Should not
                     contain trailing slash. If skinpath is not specified,
                     skins will be created in the same folder as the model.
  -skin_tga          include TGA skin along with PCX for MD2 export                    
  -flags #           set model flags, such as rocket smoke trail, rotate, etc.
                     See Quake's defs.qc for a list of all flags.
  -synctype x        set synctype flag, only used by Quake. The valid values
                     are sync (default) and rand.
  -offsets_x #       see below
  -offsets_y #       see below
  -offsets_z #       set the offsets vector, which only exists in the MDL
                     format and is not used by Quake. It's only supported here
                     for reasons of completeness.
  -renormal          recalculate vertex normals.
  -rename_frames     rename all frames to "frame1", "frame2", etc.
  -force             force "yes" response to all confirmation requests
                     regarding overwriting existing files or creating
                     nonexistent paths.

Examples:

modelconv -skin_tga -i object.mdl models/object/tris.md2 -skinpath models/object/

Converts the model object.mdl to models/object/tris.md2.  Converts skin to the Quake2 palette in PCX format. 
Sets skinpath to "models/object/" and exports TGA version of skin(s).

modelconv -i tris.md2 out.mdl

Converts the model tris.md2 to out.mdl.  All textures will be converted to the Quake palette (properly taking fullbrights into account).

modelconv -i model.md3 -tex texture.jpg -texwidth 300 -texheight 200 out.mdl

Imports the model model.md3, loads into it the image texture.jpg (resampled to 300x200), and exports it to MDL, converting the texture to the Quake palette.
